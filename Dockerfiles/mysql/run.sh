#! /usr/bin/env bash
set -eu

tail --pid $$ -F /var/log/mysql/query.log /var/log/mysql/slow.log &
exec mysqld
