### INSTALLATION DOCKER MYSQL
```
sudo docker build Dockerfiles/mysql5.1 -t mysql-5.1-log
sudo docker run --name some-mysql-5.1 -p 3307:3306 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql-5.1-log
sudo docker exec -it some-mysql-5.1  tail -f /var/log/mysql/query.log

```
### INSTALLATION DOCKER MYSQL
```
sudo docker build Dockerfiles/mysql -t mysql-5.6-log
sudo docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql-5.6-log
sudo docker exec -it some-mysql  tail -f /var/log/mysql/query.log

```
### INSTALLATION DOCKER MYSQL
```
sudo docker build --build-arg ENV_MYSQL_MULTIPLE_DATABASE=boilerplate --build-arg ENV_MYSQL_LOG=active --build-arg ENV_MYSQL_USER=root Dockerfiles/mysql -t mysql-boilerplate
sudo docker run --name mysql-boilerplate -p 3307:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql-boilerplate
sudo docker exec -it mysql-boilerplate tail -f /var/log/mysql/query.log

```
### INSTALLATION DOCKER PHP 5.2 XDEBUG
```
sudo docker build Dockerfiles/php-5.2 -t php-5-2-xdebug
sudo docker run --name php-5-2-docker  -v $PWD:/var/www/html -d php-5-2-xdebug

```
### INSTALLATION DOCKER PHP 5.6 XDEBUG
```
sudo docker build Dockerfiles/xdebug -t php-5.6-xdebug
sudo docker run --name php-5-6-docker  -v $PWD:/var/www -d php-5-6-xdebug

```
### INSTALLATION DOCKER PHP 5 ARTISAN XDEBUG
```
sudo docker build Dockerfiles/laravel-xdebug -t artisan-5-xdebug
sudo docker run --name laravel-5-docker  -v $PWD:/var/www -d artisan-5-xdebug
```

### INSTALLATION DOCKER PHP 7.0 ARTISAN XDEBUG
```
sudo docker build Dockerfiles/laravel-xdebug-php7.0 -t artisan-7.0-xdebug
sudo docker run --name artisan-xdebug-7 --network="red-ip-fija"  -v $PWD:/var/www -p 82:80 -d artisan-7.0-xdebug

```
### INSTALLATION DOCKER PHP 7.1 ARTISAN XDEBUG
```
sudo docker build Dockerfiles/laravel-xdebug-php7.1  -t artisan-7-1-xdebug
sudo docker run --name artisan-7-1-docker  -v $PWD:/var/www -d artisan-7-1-xdebug

```
### INSTALLATION DOCKER PHP 7.1 XDEBUG
```
sudo docker build Dockerfiles/xdebug-php7.1 -t xdebug-php7.1
sudo docker run --name xdebug-php7.1-docker  -v $PWD:/var/www -d php-7.1-xdebug

```
### INSTALLATION DOCKER PHP 7.3 XDEBUG
```
sudo docker build Dockerfiles/xdebug-php7.3 -t xdebug-php7.3
sudo docker run --name xdebug-php7.3-docker  -v $PWD:/var/www -d xdebug-php7.3

```
### INSTALLATION DOCKER PHP 7.2 ARTISAN XDEBUG
```
sudo docker build Dockerfiles/laravel-xdebug-php7.2 -t artisan-7-2-xdebug
sudo docker run --name artisan-xdebug-7 -v $PWD:/var/www -d artisan-7-2-xdebug
```
### INSTALLATION DOCKER PHP 7.3 ARTISAN XDEBUG
```
sudo docker build Dockerfiles/laravel-xdebug-php7.3 -t artisan-7-3-xdebug
sudo docker run --name artisan-xdebug-7-3 --network="red-ip-fija"  -v $PWD:/var/www -p 82:80 -d artisan-7-3-xdebug
```

### INSTALLATION DOCKER PHP 8.1 LARAVEL-SUPERVISOR XDEBUG
```
sudo docker build Dockerfiles/laravel-supervisor -t laravel-supervisor
sudo docker run -d --rm  --hostname=${PWD##*/}  --name=${PWD##*/} --volume $PWD:/var/www -e HRROLE=backend laravel-supervisor && sudo docker logs -f ${PWD##*/}
```
horizon
```
sudo docker run -d --rm  --hostname=${PWD##*/}  --name=${PWD##*/} --volume $PWD:/var/www -e HRROLE=backend-horizon laravel-supervisor && sudo docker logs -f ${PWD##*/}
```
### INSTALLATION DOCKER NODE-YARN
```
sudo docker build Dockerfiles/yarn-node -t yarn-node
sudo docker run --name yarn-node  -v $PWD:/workspace -d yarn-node
```
